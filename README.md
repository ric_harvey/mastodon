# The AWSCommunity Mastodon Instance

This mastodon instance is a Community driven effort to unite people interested in AWS (Amazon Web Services), so that they may share thoughts, ideas and knowledge about AWS. It is not in any way funded by amazon nor officially endorsed by them.

We want this to be a lively and friendly community with lots of tech chat and sharing of AWS knowledge. Please help spread the word about our space on the internet on other social networks and lets make this the place for AWS chat.

You can access this instance here: [https://awscommunity.social](https://awscommunity.social)

### Support Us

If you can help us pay for the service each month and support the continuation of the server that would be most welcome by all of us here, please visit our patreon to help.

[https://www.patreon.com/awscommunitysocial](https://www.patreon.com/awscommunitysocial)

### Admins + Owners

- [@Ric](https://awscommunity.social/@Ric)
- [@Gyja](https://awscommunity.social/@Gyja)
- [@Quinnypig](https://awscommunity.social/@Quinnypig)

### Moderators

- [@dfrasca80](https://awscommunity.social/@dfrasca80)


